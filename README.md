# Getting Started with the DSE (Dockerized Swagger Editor)

The intent of the Dockerized Swagger Editor is to make it so your editor is:

- Easily available
- Portable

The portability will be figured out later by mapping directories for the embedded Swagger Editor app.

The dockerized version defaults to run on port `7500`.
